class AddPositionToPlayers < ActiveRecord::Migration[5.0]
  def change
    add_column :players, :position, :string
    add_column :players, :salary, :decimal
    add_column :players, :contract, :integer
  end
end
