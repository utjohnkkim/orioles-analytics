class CreatePitchdata < ActiveRecord::Migration[5.0]
  def change
    create_table :pitchdata do |t|
      t.string :game_id
      t.string :game_date
      t.integer :year
      t.string :sv_pitch_id
      t.integer :at_bat_number
      t.integer :pitch_number
      t.integer :inning
      t.binary :top_inning_sw
      t.string :event_type
      t.string :event_result
      t.integer :pre_balls
      t.integer :pre_strikes
      t.integer :pre_outs
      t.string :throws
      t.string :pitcher_id
      t.float :initial_speed
      t.string :pitch_type
      t.float :break_x
      t.float :break_z
      t.float :plate_x
      t.float :plate_z
      t.string :HitTrajectory

      t.timestamps
    end
  end
end
