class CreatePlayers < ActiveRecord::Migration[5.0]
  def change
    create_table :players do |t|
      t.string :name
      t.string :primary_photo_path
      t.date :date_of_birth
      t.text :details

      t.timestamps
    end
  end
end
