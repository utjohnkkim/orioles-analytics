# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170224030206) do

  create_table "pitchdata", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "game_id"
    t.string   "game_date"
    t.integer  "year"
    t.string   "sv_pitch_id"
    t.integer  "at_bat_number"
    t.integer  "pitch_number"
    t.integer  "inning"
    t.binary   "top_inning_sw", limit: 65535
    t.string   "event_type"
    t.string   "event_result"
    t.integer  "pre_balls"
    t.integer  "pre_strikes"
    t.integer  "pre_outs"
    t.string   "throws"
    t.string   "pitcher_id"
    t.float    "initial_speed", limit: 24
    t.string   "pitch_type"
    t.float    "break_x",       limit: 24
    t.float    "break_z",       limit: 24
    t.float    "plate_x",       limit: 24
    t.float    "plate_z",       limit: 24
    t.string   "HitTrajectory"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "players", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.string   "primary_photo_path"
    t.date     "date_of_birth"
    t.text     "details",            limit: 65535
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.string   "position"
    t.decimal  "salary",                           precision: 10
    t.integer  "contract"
  end

end
