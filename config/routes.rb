Rails.application.routes.draw do
  get 'users/new'

  root 'search#index'
  get 'search' => "search#index", :as => :search
  resources :players
 	resources :searches
end