module ApplicationHelper
  def cl_photo_url(url, transformation)
    trans = transformation.to_s

    blank_path = false
    #this is the default returned by 
    #the default_url method in image_uploader.rb
    if url == "/assets/defaults/.jpg" #make sure this matches default_url
      blank_path = true
    end

    final_url = ""
    if url.blank? or blank_path
      final_url = "/assets/defaults/#{trans}.jpg" 
    else
      final_url = Cloudinary::Utils.cloudinary_url(url, :transformation => transformation, :type => "fetch")
    end

    return final_url
  end
end
