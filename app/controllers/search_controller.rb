class SearchController < ApplicationController
	def index
	  @search = Search.new(params, :per_page => (params[:per_page] ||= session[:per_page]))
    @pitch_data = Pitch.event_outcome_pitcher(params[:outcome])
    @pitcher1_data = @pitch_data.first
    @pitcher2_data = @pitch_data.last
    
    respond_to do |format|
      format.html
      format.js
    end
	end

	def search

  if params[:q].nil?
    @players = []
  else
    @players = Player.search params[:q]
  end
	end
end
