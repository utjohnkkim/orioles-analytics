class Search 
	attr_accessor :args

  def initialize(*params)
    @args = Hash.new.tap do |a|
      params.each do |p|
        p.each do |k,v|
          a[k.to_sym] = v
        end
      end
    end
  end

  def url_builder(options={})
    new_args = args.merge(options).delete_if {|k,v| true if ([:action,:controller,:page,:per_page,:account_id].index(k) || v.blank?) }
    new_args[:per_page] = options[:per_page] if options[:per_page]
    link = ""

    if options[:tags]
      link << "/#{options[:tags].map {|t| t.slug }.join("+")}"
    elsif new_args[:tags]
      link << "/#{new_args[:tags]}"
    else
      link << "/search"
    end

    new_args = new_args.delete_if {|k| true if (k == :tags ) }

    link << "?#{new_args.to_query}" unless new_args.to_query.blank?
    link 
  end

  def facet_position
    r = []
    r << {:name => "All", :url => url_builder(:position => nil), :active => (args[:position] ? false : true)}
    r << {:name => "SP",
          :url => url_builder(:position => "SP"), :active => (args[:postion] == "SP" ? true : false)}
    r << {:name => "RP",
          :url => url_builder(:position => "RP"), :active => (args[:postion] == "RP" ? true : false)}
    r << {:name => "1st Baseman",
          :url => url_builder(:position => "1b"), :active => (args[:postion] == "1b" ? true : false)}
    r << {:name => "2nd Baseman",
          :url => url_builder(:position => "2b"), :active => (args[:postion] == "2b" ? true : false)}
     r << {:name => "3rd Baseman",
          :url => url_builder(:position => "3b"), :active => (args[:postion] == "3b" ? true : false)}
    r
  end
  
  def facet_contract_remaining
    r = []
    r << {:name => "All", :url => url_builder(:years => nil), :active => (args[:position] ? false : true)}
    r << {:name => "Pending Free Agent",
          :url => url_builder(:years => "1"), :active => (args[:postion] == "1" ? true : false)}
    r << {:name => "2 Years Left",
          :url => url_builder(:years => "2"), :active => (args[:postion] == "2" ? true : false)}
    r << {:name => "3 Years Left",
          :url => url_builder(:years => "3"), :active => (args[:postion] == "3" ? true : false)}
    r << {:name => "4 Years Left",
          :url => url_builder(:years => "4"), :active => (args[:postion] == "4" ? true : false)}
     r << {:name => "5 Years Left",
          :url => url_builder(:years => "5"), :active => (args[:postion] == "5" ? true : false)}
    r
  end


  def facet_salary
    r = []
    r << {:label => "All", :url => url_builder(salary_low: nil, salary_high: nil), :active => (args[:position] ? false : true)}
    r << {label: "Under $500,000",
          :url => url_builder(salary_low: 1, salary_high: 500000), :active => (args[:postion] == "SP" ? true : false)}
    r << {label: "$500,001 - $1,000,000",
          :url => url_builder(salary_low: 500000, salary_high: 1000000), :active => (args[:postion] == "RP" ? true : false)}
    r << {label: "$1,000,001 - $3,000,000",
          :url => url_builder(salary_low: 1000000, salary_high: 3000000), :active => (args[:postion] == "1b" ? true : false)}
    r << {label: "$3,000,000 - $5,000,000",
          :url => url_builder(salary_low: 3000000, salary_high: 5000000), :active => (args[:postion] == "2b" ? true : false)}
    r << {label: "$5,000,000 - $10,000,000",
          :url => url_builder(salary_low: 5000000, salary_high: 10000000), :active => (args[:postion] == "3b" ? true : false)}
    r << {label: "$10,000,000 - $15,000,000",
          :url => url_builder(salary_low: 10000000, salary_high: 15000000), :active => (args[:postion] == "1b" ? true : false)}
    r << {label: "$15,000,000 - $20,000,000",
          :url => url_builder(salary_low: 15000000, salary_high: 20000000), :active => (args[:postion] == "2b" ? true : false)}
     r << {label: "Over $20,000,000" ,
          :url => url_builder(salary_low: 20000000), :active => (args[:postion] == "3b" ? true : false)}
    r
  end

  def facet_outcomes
    r = []
    r << {label: "Probability of Home Run",
          :url => url_builder(outcome: "home_run"), :active => (args[:postion] == "home_run" ? true : false)}
    r << {label: "Probability of Strikeout",
          :url => url_builder(outcome: "strikeout"), :active => (args[:postion] == "strikeout" ? true : false)}
    r
  end



  def players
    return @players if @players
    unless player_query.nil?
	    @players = Player.search((args[:new] ? player_query_new : player_query))
	  end
    return @players
  end

  def player_query
    s = {:explain => false, :sort => [], :query => {"bool" => { "must" => [], "should" => [], "must_not" => [], "filter" => [] }}}

    if args[:position].blank?
	    s = {:explain => false, :sort => [], :query => {"bool" => { "must" => [], "should" => [], "must_not" => [], "filter" => [] }}}
    elsif args[:position] == "3b"
	    s[:query]["bool"]["filter"] << {:match => {:position => "5"}}
    elsif args[:position] == "SP"
    	s[:query]["bool"]["filter"] << {:match => {:position => "1"}}
    end

    
  	s[:query]["bool"]["filter"] << {range: {:salary => {gte: args[:salary_low].to_i }}} unless args[:salary_low].blank?
  	s[:query]["bool"]["filter"] << {range: {:salary => {lte: args[:salary_high].to_i }}} unless args[:salary_high].blank?
   
    return s
  end

  def cat

  end

end
