class Player < ApplicationRecord
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks

  has_many :pitches

	def self.positions
		[
	    {position: 'sp', label: "Starting Pitcher" },
	    {position: 'rp', label: "Relief Pitcher" },
	    {position: '1b', label: "First Base" },
	    {position: '2b', label: "Second Base" },
	    {position: '3b', label: "Third Base" },
	    {position: 'c',  label: "Catcher" },
	    {position: 'lf', label: "Left Fielder" },
	    {position: 'cf', label: "Center Fielder" },
	    {position: 'rf', label: "Right Fielder" },
    ]
	end



	def self.contract_remaining
		[
	    {left: 1, label: "Pending Free Agent" },
	    {left: 2, to: 1000000, label: "2 Years Left" },
	    {left: 3, to: 3000000, label: "3 Years Left" },
	    {left: 4, to: 5000000, label: "4 Years Left" },
	    {left: 5, to: 10000000, label: "5 Years Left" },
	    {left: 6, to: 15000000, label: "6 Years Left" },
	    {left: 7, to: 20000000, label: "7 Years Left" },	    
    ]
	end

  def self.setup_index(test=false)
    Player.__elasticsearch__.delete_index! rescue puts "Index doesn't exist"
    Player.__elasticsearch__.create_index! rescue puts "Index doesn't exist"

    if test
      ll=[]
      Player.limit(10).each do |s|
        r=s.__elasticsearch__.index_document
        puts "-------------------"
        ll << r
        puts "-------------------"
      end
      ll.each {|l| puts l.inspect }
    else
      Player.import(force:true)
    end
  end
end
