class Pitch < ApplicationRecord
	belongs_to :player

	def self.event_outcome_pitcher(param)					
			param = "home_run" if param.nil? 
	
			array_xy_1 = []
			array_xy_2 = []
			(0..9).each do |x|
				(0..9).each do |y|
					array_xy_1 << [x,y]
					array_xy_2 << [x,y]
				end
			end

			point = 0
			pos_1 = -1.78
			pos_2 = -1.58
			pos_3 = 0.6
			pos_4 = 1
			a = 0
			b = 9
			arr_pos = 0
		
			10.times do 
				pos_1 += 0.28
				pos_2 += 0.28
				10.times do		

					pos_3 += 0.4
					pos_4 += 0.4
					strikeout_data = self.where("event_result = ?", "strikeout")
					home_run_data = self.where("event_result = ?", "home_run")
					player_1_data = self.where(:player_id => "501957")
					player_2_data = self.where(:player_id => "592332")

					if param == "strikeout"
						pitcher_1_strikeout = strikeout_data.where("player_id = ?", 501957)
						strike_out_count_1 = pitcher_1_strikeout.where(plate_x: pos_1..pos_2).where(plate_z: pos_3..pos_4).where(:event_result => "strikeout").count
						pitch_count_1 = player_1_data.where(plate_x: pos_1..pos_2).where(plate_z: pos_3..pos_4).count
						point_1 = strike_out_count_1.fdiv(pitch_count_1).round(5)

						pitcher_2_strikeout = strikeout_data.where("player_id = ?", 592332)
						strike_out_count_2 = pitcher_2_strikeout.where(plate_x: pos_1..pos_2).where(plate_z: pos_3..pos_4).count
						pitch_count_2 = player_2_data.where(plate_x: pos_1..pos_2).where(plate_z: pos_3..pos_4).count
						point_2 = strike_out_count_2.fdiv(pitch_count_2).round(5)
					elsif param == "home_run"
						pitcher_1_homerun = home_run_data.where("player_id = ?", 501957)
						home_run_count_1 = pitcher_1_homerun.where(plate_x: pos_1..pos_2).where(plate_z: pos_3..pos_4).where(:event_result => "home_run").count
						pitch_count_1 = player_1_data.where(plate_x: pos_1..pos_2).where(plate_z: pos_3..pos_4).count
						point_1 = home_run_count_1.fdiv(pitch_count_1).round(5)

						pitcher_2_homerun = home_run_data.where("player_id = ? ", 592332)
						home_run_count_2 = pitcher_2_homerun.where(plate_x: pos_1..pos_2).where(plate_z: pos_3..pos_4).count
						pitch_count_2 = player_2_data.where(plate_x: pos_1..pos_2).where(plate_z: pos_3..pos_4).count
						point_2 = home_run_count_2.fdiv(pitch_count_2).round(5)
					end
					if pitch_count_1 < 9
						array_xy_1[arr_pos] << 0
						array_xy_1[arr_pos][0] = ((pos_1+pos_2)/2).to_f.round(3)
						array_xy_1[arr_pos][1] = ((pos_3+pos_4)/2).to_f.round(3)
					else
						array_xy_1[arr_pos] << (point_1 * 100).round(2)
						array_xy_1[arr_pos][0] = ((pos_1+pos_2)/2).to_f.round(3)
						array_xy_1[arr_pos][1] = ((pos_3+pos_4)/2).to_f.round(3)
					end	
					if pitch_count_2 < 9
						array_xy_2[arr_pos] << 0
						array_xy_2[arr_pos][0] = ((pos_1+pos_2)/2).to_f.round(3)
						array_xy_2[arr_pos][1] = ((pos_3+pos_4)/2).to_f.round(3)
					else
						array_xy_2[arr_pos] << (point_2 * 100).round(2)
						array_xy_2[arr_pos][0] = ((pos_1+pos_2)/2).to_f.round(3)
						array_xy_2[arr_pos][1] = ((pos_3+pos_4)/2).to_f.round(3)
					end	
					arr_pos += 1
				end
				pos_3 = 0.6
				pos_4 = 1
			end
			array_pitch_data = []
			array_pitch_data << array_xy_1
			array_pitch_data << array_xy_2
			return array_pitch_data
	end

end
